var Sequelize = require('sequelize');
var config = require('./configdb');


//creo el objeto sequelize tomando parametros de del archivo de condif
var sequelize = new Sequelize(
    config.name,
    config.username,
    config.password,
    config.options
);

var User = sequelize.define('user', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    name: Sequelize.STRING,
    password: Sequelize.STRING,
    email: Sequelize.STRING
})


//pruebo la conexion
sequelize
    .authenticate()
    .then(function (err) {
        console.log('Connection has been established successfully.');
    })
    .catch(function (err) {
        console.log('Unable to connect to the database:', err);
    });

User.sync()
    .then(function () {
        var data = {
            email: 'hola@gmail.com',
            password: 'contrasenia'
        }
        User.create(data);
    })

